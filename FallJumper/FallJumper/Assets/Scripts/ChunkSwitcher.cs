﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChunkSwitcher : MonoBehaviour
{
    [SerializeField] private int _maxActivePanels;
    private LevelChunk[] _chunks;

    // Start is called before the first frame update
    void Start()
    {
        _chunks = GetComponentsInChildren<LevelChunk>();
        for (int i = 0; i < _chunks.Length; i++)
        {
            _chunks[i].Index = i;
            if(i > _maxActivePanels - 1)
                _chunks[i].SetActive(false);
        }
    }

    

    public void ChangeActivePanels(int index)
    {
        int min = index - _maxActivePanels;
        for (int i = min < 0 ? 0 : min; i < (index - _maxActivePanels / 2)  ; i++)
            _chunks[i].SetActive(false);
        int max = index + _maxActivePanels;
        for (int i = index + _maxActivePanels / 2; i <  (max > _chunks.Length ? _chunks.Length : max); i++)
            _chunks[i].SetActive(true);
    }
}
