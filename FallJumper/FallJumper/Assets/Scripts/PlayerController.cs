﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    [SerializeField] private float speed;
    [SerializeField] private GameObject Art;
    public float FallSpeed; // Скорость падения
    public float OldPosition; // Старая позицыя
    public float NewPosition; // Новая позицыя
    private Rigidbody2D playerRigidbody;
    private ConstantForce2D customGravity;
    private float _gravityForceAmount;

    // Start is called before the first frame update
    void Awake()
    {
        playerRigidbody = GetComponent<Rigidbody2D>();
        customGravity = GetComponent<ConstantForce2D>();
        Physics.gravity = Vector3.zero;
        _gravityForceAmount = playerRigidbody.mass * 20f;
        customGravity.force = new Vector2(-_gravityForceAmount, 0); // gravity to the left
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        FallSpeed = (OldPosition - NewPosition); //Вичесление скорости падения
        if (FallSpeed > 0.01f)
            Debug.Log(FallSpeed);
        
            OldPosition = NewPosition; // Новая позицыя стает старой
        NewPosition = transform.position.y; // Текущая позицыя стает новой
        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(Vector3.left * Time.deltaTime * speed);
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(Vector3.right * Time.deltaTime * speed);
        }
        if(Input.GetMouseButtonDown(0))
           // if()
            customGravity.force = new Vector2(customGravity.force.x < 0 ? _gravityForceAmount : -_gravityForceAmount, 0);
    }
}
