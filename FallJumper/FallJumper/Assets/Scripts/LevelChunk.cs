﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

[RequireComponent(typeof(Tilemap))]
[RequireComponent(typeof(BoxCollider2D))]
[ExecuteInEditMode]
public class LevelChunk : MonoBehaviour
{
    public int Index;
    private Tilemap _map;
    private BoxCollider2D _trigger;
    private ChunkSwitcher _chunkSwitcher;
    // Start is called before the first frame update
    void Start()
    {
        _chunkSwitcher = GetComponentInParent<ChunkSwitcher>();
        _map = GetComponent<Tilemap>();
        _trigger = GetComponent<BoxCollider2D>();
        _trigger.isTrigger = true;

        //SetTriggerSizeAndOffset();
    }


    private void SetTriggerSizeAndOffset()
    {
        var xOffset = _map.size.x * 0.5f + _map.cellBounds.center.x - 0.5f;
        var yOffset = _map.cellBounds.center.y;
        _trigger.offset = new Vector2(xOffset, yOffset);
        _trigger.size = new Vector2(1, _map.size.y);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag != "Player") return;
        _chunkSwitcher.ChangeActivePanels(Index);

    }

    public void SetActive(bool active)
    {
        gameObject.SetActive(active);
    }
}
