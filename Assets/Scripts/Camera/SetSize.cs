﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetSize : MonoBehaviour
{
    [SerializeField] private Transform _background;
    private float _wightBackground;
    private float _hightBackground;

    private void Awake()
    {
        var spriteBack = _background.GetComponent<SpriteRenderer>();

        if (spriteBack)
        {
            _wightBackground = spriteBack.bounds.size.x;
            _hightBackground = spriteBack.bounds.size.y;

            Camera.main.orthographicSize = _wightBackground / (Camera.main.aspect * 2);
        }
    }
}
