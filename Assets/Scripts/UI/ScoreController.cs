﻿using UnityEngine;
using TMPro;
using UnityEngine.Events;

public class SpeedEvent : UnityEvent<float> { }

public class ScoreController : MonoBehaviour
{
    public static SpeedEvent OnChangeLevelSpeed = new SpeedEvent();

    [SerializeField] private TextMeshProUGUI _text = null;
    [SerializeField] private int _stepPoint = 10;
    [SerializeField] private float _takeSpeed = 0.1f;

    public int Score => _score;
    private int _score;

    [SerializeField] private int _pointFromMessage = 2;

    private void OnEnable()
    {
        GenerateMessage.OnChangeSentiment.AddListener(ChangeScoreFromPlatform);
    }
    private void OnDisable()
    {
        GenerateMessage.OnChangeSentiment.RemoveListener(ChangeScoreFromPlatform);
    }

    void ChangeScoreFromPlatform()
    {
        _score += _pointFromMessage;
        _text.text = "Score: " + _score;

        if (_score % _stepPoint == 0)
            OnChangeLevelSpeed.Invoke(_takeSpeed);
    }
}
