﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;

[Serializable]
public class ControllerEvent : UnityEvent<ECharacterMovement> { }

public class ChangePlayerControl : MonoBehaviour
{
    [SerializeField] private ECharacterMovement _startCharacterMovement = ECharacterMovement.HorizontalMouse;
    public static ControllerEvent ControllerEvent = new ControllerEvent();
    public static ECharacterMovement CharacterMovement;

    private void Awake()
    {
        CharacterMovement = _startCharacterMovement;
    }

    public void JostickOn()
    {
        CharacterMovement = ECharacterMovement.Jostick;
        ControllerEvent.Invoke(ECharacterMovement.Jostick);
    }

    public void FromCenterOn()
    {
        CharacterMovement = ECharacterMovement.FromCenter;
        ControllerEvent.Invoke(ECharacterMovement.FromCenter);
    }

    public void ByMouseOn()
    {
        CharacterMovement = ECharacterMovement.ByMouse;
        ControllerEvent.Invoke(ECharacterMovement.ByMouse);
    }

    public void ByHorizontalMouseOn()
    {
        CharacterMovement = ECharacterMovement.HorizontalMouse;
        ControllerEvent.Invoke(ECharacterMovement.HorizontalMouse);
    }
}
