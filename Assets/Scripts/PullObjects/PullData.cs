﻿using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

//[CreateAssetMenu(menuName = "PullObjects/Data", fileName = "NewPull")]
public class PullData : ScriptableObject
{
    public GameObject Item;

    private List<GameObject> _items;

    public PullData()
    {
        _items = new List<GameObject>(); 
    }

    public GameObject GetAvailableItem()
    {
        if (_items.Count == 0)
            return CreateItem();

        foreach (var item in _items)
        {
            if (!item.activeSelf)
                return item;
        }
        
        return CreateItem();
    }

    public void Add(GameObject gameObject)
    {
        _items.Add(gameObject);
    }

    public int ActiveCount
    {
        get
        {
            int count = 0;
            foreach (var item in _items)
            {
                if (item.activeSelf)
                    count++;
            }
            return count;
        }
                
    }

    public int Count => _items.Count;

    private GameObject CreateItem()
    {
        GameObject item;
        item = Instantiate(Item);
        _items.Add(item);
        return item;
    }


}
