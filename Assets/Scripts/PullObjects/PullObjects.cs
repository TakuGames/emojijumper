﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PullObjects : MonoBehaviour
{
    [SerializeField] private PullObjectData _pullObjectData;
    private static PullObjectData _curConfigData;
    private static PullData _pullSmiles;
    private static PullData _pullRightMessages;
    private static PullData _pullLeftMessages;

    private void Awake()
    {
        _curConfigData = _pullObjectData;

        _pullSmiles = new PullData();
        _pullSmiles.Item = _pullObjectData.SmilePrefab;

        _pullRightMessages = new PullData();
        _pullRightMessages.Item = _pullObjectData.MessageRight;

        _pullLeftMessages = new PullData();
        _pullLeftMessages.Item = _pullObjectData.MessageLeft;
    }




    public static void AddToPullSmiles(GameObject gameObject)
    {
        _pullSmiles.Add(gameObject);
    }

    public static GameObject GetSmile(ESentiment sentiment, Vector3 position)
    {
        var emoji = _pullSmiles.GetAvailableItem();
        emoji.transform.position = position;
        emoji.GetComponent<SentimentController>().Sentiment = sentiment;
        return Activate(emoji);
    }

    public static GameObject GetSmile(ESentiment sentiment, Vector3 position, Sprite sprite)
    {
        var emoji = _pullSmiles.GetAvailableItem();
        emoji.transform.position = position;
        var component = emoji.GetComponent<SentimentController>();
        component.Sentiment = sentiment;
        component.Sprite = sprite;
        return Activate(emoji);
    }

    public static int GetActiveSmileCount() => _pullSmiles.ActiveCount;



    public static GameObject GetMessage(EDirection direction)
    {
        if (direction == EDirection.Left) return Activate(_pullLeftMessages.GetAvailableItem());
        if (direction == EDirection.Right) return Activate(_pullRightMessages.GetAvailableItem());
        return null;
    }

    public static int GetMessagesCount => _pullLeftMessages.Count + _pullRightMessages.Count;




    private static GameObject Activate(GameObject item)
    {
        item.SetActive(true);
        return item;
    }


}