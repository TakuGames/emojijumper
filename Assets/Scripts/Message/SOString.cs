﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new String", menuName = "Configs/String")]
public class SOString : ScriptableObject
{
    public string String;
    
    public string GetString(int length)
    {
        string str = "";
        char ch;
        
        for (int i = 0; i < length; i++)
        {
            ch = String[Random.Range(0, String.Length)];
            str += ch;
        }

        return str;
        // return String[Random.Range(0, String.Length)];
    }
}


