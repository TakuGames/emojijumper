﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ETypeContent
{
    String = 0,
    Smile,
    Food,
    Count = 3,
    Decor,
    Char,
    Non
}
