﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class StringMessage : AItemMessage
{
    private TextMeshProUGUI _text;

    public override float width => _text.preferredWidth;

    protected override void Apply()
    {
        _text = GetComponent<TextMeshProUGUI>();
        _message = GetComponentInParent<GenerateMessage>();
        _rect = GetComponent<RectTransform>();
        _apply = true;

    }

    public float GenerateItem(string str)
    {
        if (!_apply)
            Apply();
        
        _text.text = str;
        gameObject.SetActive(true);
        return _text.preferredWidth;
    }

    public float GenerateItem(float availableLength)
    {
        if (!_apply)
            Apply();
        
        string txt = _message.GetString();
        _text.text = txt;
        if (_text.preferredWidth > availableLength)
            return 0;
        
        gameObject.SetActive(true);
        return _text.preferredWidth;
    }
}
