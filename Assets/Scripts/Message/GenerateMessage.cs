﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class GenerateMessage : MonoBehaviour, ISerializationCallbackReceiver
{
    public static Action OnTakePlatformPoint = () => { };
    public static UnityEvent OnChangeSentiment = new UnityEvent();
    
    [SerializeField] private Image _avatar;
    private int _avatarIndex;
    
    [Header("Strips")]
    [SerializeField] [Range(3, 10)] private int _minLengthStrip = 3;
    [SerializeField] [Range(3, 10)] private int _maxLengthStrips = 10;
    private List<StripMessage> _curStrips = new List<StripMessage>();
    private StripMessage[] _strips;

    [Header("Content")]
    [SerializeField] private RectTransform _content;
    [SerializeField] private float _spacing = 0.2f;
    [SerializeField] private float _minWidthElementForContent = 0.7f;
    public float spacing => _spacing;

    [Header("Sentiment")]
    [SerializeField] private SOString _stringGood;
    [SerializeField] private SOString _stringBad;
    private ESentiment _sentiment;

    [Header("Cell")]
    [SerializeField] private int _maxLengthCell = 8;

    [Header("Test")]
    [SerializeField] private bool _test;

    private List<EmojiMessage> _badSmiles = new List<EmojiMessage>();
    private List<EmojiMessage> _badFood = new List<EmojiMessage>();

    public void AddBadSmile(EmojiMessage smile)
    {
        _badSmiles.Add(smile);
    }

    public void AddBadFood(EmojiMessage food)
    {
        _badFood.Add(food);
    }

    public void RemoveFood(EmojiMessage food)
    {
        _badFood.Remove(food);
        if (_badFood.Count <= 0)
        {
            foreach (var smile in _badSmiles)
            {
                smile.ChangeTypeToGood();
            }

            _avatar.sprite = Resource.GetAvatarByIndex(_avatarIndex, ESentiment.Good);
           // OnTakePlatformPoint();
            OnChangeSentiment.Invoke();
        }
    }
    
    public ESentiment Sentiment
    {
        get => _sentiment;
        set => _sentiment = value;
    }
    
    private SpriteRenderer _sr;
    private RectTransform _canvas;
    private BoxCollider2D _collider;

    public string GetString()
    {
        int length = Random.Range(1, _maxLengthCell);
        
        switch (_sentiment)
        {
            case ESentiment.Good:
                return _stringGood.GetString(length);
            case ESentiment.Bad:
                return _stringBad.GetString(length);
        }
        return null;
    }

    public void OnBeforeSerialize(){}

    public void OnAfterDeserialize()
    {
        if (_minLengthStrip >= _maxLengthStrips)
            _minLengthStrip = Mathf.Clamp(_maxLengthStrips - 1, 3,10);

        if (_maxLengthStrips <= _minLengthStrip)
            _maxLengthStrips = Mathf.Clamp(_minLengthStrip + 1, 3, 10);
    }
    
    private void Awake()
    {
        _strips = GetComponentsInChildren<StripMessage>(true);
        _sr = GetComponent<SpriteRenderer>();
        _canvas = GetComponentInChildren<Canvas>().GetComponent<RectTransform>();
        _collider = GetComponent<BoxCollider2D>();
    }

    private void Start()
    {
        if (_test)
            Generate(50);
    }

    public Vector2 Generate(int chanceBad)
    {
        _sentiment = Random.Range(0, 101) <= chanceBad ? ESentiment.Bad : ESentiment.Good;
        
        if (_sentiment == ESentiment.Bad)
        {
            _badFood.Clear();
            _badSmiles.Clear();
        }
        
        _curStrips.Clear();
        int stripsCount = Random.Range(1, _strips.Length);
        float stripLenght = 0;
        float curStripLenght = 0;
        
        PlaceAvatar(stripsCount > 1);
        
        for (int i = 0; i < _strips.Length; i++)
        {
            if (i < stripsCount)
            {
                _strips[i].gameObject.SetActive(true);
                curStripLenght = _strips[i].GenerateContentInStrip();
                stripLenght = curStripLenght > stripLenght ? curStripLenght : stripLenght;
                _curStrips.Add(_strips[i]);
            }
            else
            {
                _strips[i].gameObject.SetActive(false);
            }
        }

        stripLenght += 2f;
        Vector2 size = new Vector2(stripLenght, stripsCount);
        _sr.size = size;
        _canvas.sizeDelta = size;
        _collider.offset = new Vector2(0, -_sr.bounds.extents.y);
        _collider.size = new Vector3(stripLenght, 0.2f);
        
        return size;
    }
    
//    public Vector2 Generate()
//    {
//        _sentiment = (ESentiment) Random.Range(0, 2);
//        
//        if (_sentiment == ESentiment.Bad)
//        {
//            _badFood.Clear();
//            _badSmiles.Clear();
//        }
//        
//        _curStrips.Clear();
//        var stripsCount = Random.Range(1, _strips.Length);
//        var stripsLenght = Random.Range(_minLengthStrip, _maxLengthStrips + 1);
//
//        PlaceAvatar(stripsCount > 1);
// 
//        Vector2 size = new Vector2(stripsLenght, stripsCount);
//        _sr.size = size;
//        _canvas.sizeDelta = size;
//        _collider.offset = new Vector2(0, -_sr.bounds.extents.y);
//        _collider.size = new Vector3(stripsLenght, 0.2f);
//        
//        for (int i = 0; i < _strips.Length; i++)
//        {
//            if (i < stripsCount)
//            {
//                _strips[i].gameObject.SetActive(true);
//                _strips[i].GenerateContentInStrip();
//                //_strips[i].GenerateContentInStrip(_content.rect.width, _minWidthElementForContent, _spacing);
//                _curStrips.Add(_strips[i]);
//            }
//            else
//            {
//                _strips[i].gameObject.SetActive(false);
//            }
//        }
//
//        return size;
//    }

    public void PlaceAvatar(bool activate)
    {
        if (activate)
        {
            _avatar.sprite = Resource.GetRandomAvatar(out _avatarIndex, _sentiment);
            _avatar.gameObject.SetActive(true);
        }
        else
        {
            _avatar.gameObject.SetActive(false);
        }
    }
}
