﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;

public class ContactWithEnemy : MonoBehaviour
{
    private EmojiMessage _emoji;
    private GenerateMessage _message;
    private Image _image;
    private Collider2D _collider;

    private void Start()
    {
        _emoji = GetComponent<EmojiMessage>();
        _image = GetComponent<Image>();
        _collider = GetComponent<Collider2D>();
        _message = GetComponentInParent<GenerateMessage>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!other.CompareTag("Player"))
            return;

        switch (_emoji.type)
        {
            case ETypeContent.Food:
                var health = other.GetComponent<Health>();
                ContactFood(ref health);
                ShowDecor();
                break;
//            case ETypeContent.Trap:
//                ContactTrap(ref otherSentiment);
//                break;
            case ETypeContent.Smile:
                PullObjects.GetSmile(ESentiment.Good, gameObject.transform.position);
                ShowDecor();
                break;
        }

       // gameObject.SetActive(false);
    }

    private void ShowDecor()
    {
        _image.sprite = Resource.GetRandomTrapSprite(ESentiment.Good);
        _emoji.type = ETypeContent.Decor;
        _collider.enabled = false;
    }

    private void ContactFood(ref Health health)
    {
        if (_message.Sentiment == ESentiment.Good)
            health.AddHealth();
        else
        {
            health.RemoveHealth();
            _message.RemoveFood(_emoji);
        }



//        if (otherSentiment.Sentiment == _message.Sentiment)
//        {
//            otherSentiment.GetComponent<Health>().AddHealth();
//            return;
//        }
//
//        if (_message.Sentiment == ESentiment.Bad)
//        {
//            otherSentiment.Sentiment = ESentiment.Bad;
//            return;
//        }
//        
//        if (_message.Sentiment == ESentiment.Good)
//        {
//            otherSentiment.Sentiment = ESentiment.Good;
//        }
    }


    private void ContactTrap(ref SentimentController otherSentiment)
    {
        if (otherSentiment.Sentiment != _message.Sentiment)
        {
            otherSentiment.gameObject.SetActive(false);
            GameController.GameOverCheck();
        }
    }
}