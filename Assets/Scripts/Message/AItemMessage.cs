﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AItemMessage : MonoBehaviour
{
    protected RectTransform _rect;
    protected bool _apply = false;
    protected GenerateMessage _message;
    private float _position;

    public RectTransform GetRect => _rect;

    public void SetPosition(float position)
    {
        var newPos = _rect.anchoredPosition;
        newPos.x = position;
        _rect.anchoredPosition = newPos;
        _position = position;
    }

    public abstract float width { get; }
    protected abstract void Apply();
}