﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EmojiMessage : AItemMessage
{
    private Image _image;
    private ETypeContent _type;
    private Collider2D _collider;

    public ETypeContent type
    {
        get => _type;
        set => _type = value;
    }

    public override float width => _rect.rect.width;

    protected override void Apply()
    {
        _rect = GetComponent<RectTransform>();
        _message = GetComponentInParent<GenerateMessage>();
        _image = GetComponent<Image>();
        _collider = GetComponent<Collider2D>();
        _apply = true;
    }

    public float GenerateItem(ETypeContent type)
    {
        if (!_apply)
            Apply();

        gameObject.SetActive(true);
        _collider.enabled = true;
        
        _type = type;
        switch (_type)
        {
            case ETypeContent.Smile:
                _image.sprite = Resource.GetRandomSmileSprite(_message.Sentiment);
                if (_message.Sentiment == ESentiment.Bad)
                {
                    _collider.enabled = false;
                    _message.AddBadSmile(this);
                }
                break;
            case ETypeContent.Food:
                _image.sprite = Resource.GetRandomFoodSprite(_message.Sentiment);
                if (_message.Sentiment == ESentiment.Bad)
                {
                    _message.AddBadFood(this);
                }
                break;
//            case ETypeContent.Trap:
//                _image.sprite = PullObjects.GetRandomTrapSprite(_message.Sentiment);
//                break;
        }
        
        return _rect.sizeDelta.x;
    }

    public void ChangeTypeToGood()
    {
        _image.sprite = Resource.GetRandomSmileSprite(ESentiment.Good);
        _collider.enabled = true;
    }
}