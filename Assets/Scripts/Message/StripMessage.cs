﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class StripMessage : MonoBehaviour
{
    private StringMessage[] _strings;
    private EmojiMessage[] _emojis;
    private List<AItemMessage> _curItems = new List<AItemMessage>();
    private bool _apply = false;
    private GenerateMessage _message;

    private void Apply()
    {
        _strings = GetComponentsInChildren<StringMessage>(true);
        _emojis = GetComponentsInChildren<EmojiMessage>(true);
        _message = GetComponentInParent<GenerateMessage>();
        _apply = true;
    }

    public float GenerateContentInStrip()
    {
        if (!_apply)
            Apply();
        
        ResetItems();
        float width = 0;

        foreach (var content in ConvertReplicas.GetRandomStrip(_message.Sentiment))
        {
            switch (content.type)
            {
                case ETypeContent.String:
                    foreach (var str in _strings)
                    {
                        if (!str.gameObject.activeSelf)
                        {
                            width += str.GenerateItem(content.word);
                            _curItems.Add(str);
                            break;
                        }
                    }
                    break;
                default:
                    foreach (var emoji in _emojis)
                    {
                        if (!emoji.gameObject.activeSelf)
                        {
                            width += emoji.GenerateItem(content.type);
                            _curItems.Add(emoji);
                            break;
                        }
                    }
                    break;
            }

            width += _message.spacing;
        }
        
        PositionItems();

        return width;
    }

    public void GenerateContentInStrip(float width, float minWidth, float spacing)
    {
        if (!_apply)
            Apply();
        
        ResetItems();

        float subtractWidth = 0;

        while (width >= minWidth)
        {
            ETypeContent type = (ETypeContent) Random.Range(0, 2);
            if (type == ETypeContent.Smile)
                type = (ETypeContent) Random.Range(1, (int)ETypeContent.Count);
            
            switch (type)
            {
                case ETypeContent.String:
                    foreach (var cell in _strings)
                    {
                        if (!cell.gameObject.activeSelf)
                        {
                            subtractWidth = cell.GenerateItem(width);
                            if (subtractWidth <= 0)
                                break;
                            
                            _curItems.Add(cell);
                            break;
                        }
                    }
                    break;
                default:
                    foreach (var emoji in _emojis)
                    {
                        if (!emoji.gameObject.activeSelf)
                        {
                            subtractWidth = emoji.GenerateItem(type);
                            _curItems.Add(emoji);
                            break;
                        }
                    }
                    break;
            }

            subtractWidth += spacing;
            width -= subtractWidth;
        }

        PositionItems();
    }

    private void PositionItems()
    {
        float position = 0;
        for (int i = 0; i < _curItems.Count; i++)
        {
            //_curItems[i].transform.SetSiblingIndex(i);
            _curItems[i].SetPosition(position);
            position += _curItems[i].width + _message.spacing;
        }
    }

    private void ResetItems()
    {
        if (_curItems.Count > 0)
        {
            foreach (var item in _curItems)
            {
                item.gameObject.SetActive(false);
            }

            _curItems.Clear();
        }
    }
}