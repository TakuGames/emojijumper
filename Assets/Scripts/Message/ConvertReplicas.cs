﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public struct Content
{
    public ETypeContent type;
    public string word;
}

public class ConvertReplicas : MonoBehaviour
{
//    [SerializeField] private string _textGood = "Assets/Resources/Replicas/Провода.txt";
//    [SerializeField] private string _textBad = "Assets/Resources/Replicas/Провода2.txt";
    [SerializeField] private TextAsset _textBad;
    [SerializeField] private TextAsset _textGood;
    private static List<string> _replicasGood = new List<string>();
    private static List<string> _replicasBad = new List<string>();

    [SerializeField] private SOCharConfig _charConfig;
    private static SOCharConfig _curCharConfig;

    private void GenerateReplicas(TextAsset text, ref List<string> replicas)
    {
        var test = text.ToString();
        var array = test.Split('\n');

        foreach (var t in array)
        {
            replicas.Add(t);
        }
    }

    private void Awake()
    {
        _curCharConfig = _charConfig;
        GenerateReplicas(_textGood, ref _replicasGood);
        GenerateReplicas(_textBad, ref _replicasBad);
    }

    public static List<Content> GetRandomStrip(ESentiment sentiment)
    {
        List<Content> strip = new List<Content>();

        if (sentiment == ESentiment.Good)
            GetRandomStripBySentinment(ref strip, ref _replicasGood);
        else
            GetRandomStripBySentinment(ref strip, ref _replicasBad);

        return strip;
    }

    private static void GetRandomStripBySentinment(ref List<Content> strip, ref List<string> replicas)
    {
        int r = Random.Range(0, replicas.Count);

        Content curContent = new Content();
        curContent.type = ETypeContent.Non;
        ETypeContent curType;

        for (int i = 0; i < replicas[r].Length; i++)
        {
            curType = _curCharConfig.GetTypeByChar(replicas[r][i]);

            if (curType == ETypeContent.String)
            {
                if (curContent.type == ETypeContent.String)
                {
                    curContent.word += replicas[r][i];
                }
                else
                {
                    curContent.type = curType;
                    curContent.word = "" + replicas[r][i];
                }

                continue;
            }

            if (curContent.type == ETypeContent.String)
            {
                strip.Add(curContent);
            }

            curContent.type = curType;
            curContent.word = "";
            strip.Add(curContent);
        }

        if (curContent.type == ETypeContent.String)
            strip.Add(curContent);
    }
}