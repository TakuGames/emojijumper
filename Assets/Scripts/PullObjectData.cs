﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

[CreateAssetMenu(fileName = "new Pull Object Data", menuName = "Configs/PullObjectData")]
public class PullObjectData : ScriptableObject
{
    [Header("Emoji Prefabs")]
    public GameObject SmilePrefab;

    [Header("Messages")]
    public GameObject MessageRight;
    public GameObject MessageLeft;
}
