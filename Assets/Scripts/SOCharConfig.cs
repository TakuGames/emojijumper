﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public struct CharType
{
    public char Char;
    public ETypeContent Content;
}


[CreateAssetMenu(fileName = "new Char Config", menuName = "Configs/CharConfig")]
public class SOCharConfig : ScriptableObject
{
    [SerializeField] private CharType[] _charTypes;

    public ETypeContent GetTypeByChar(char findChar)
    {
        foreach (var ch in _charTypes)
        {
            if (ch.Char == findChar)
                return ch.Content;
        }

        return ETypeContent.String;
    }
}
