﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Resource : MonoBehaviour
{
    [SerializeField] private ResourceData _resourceData;
    private static ResourceData _curConfigData;
    private static ConvertReplicas _replicas;
    private void Awake()
    {
        _curConfigData = _resourceData;
        _replicas = GetComponent<ConvertReplicas>();
    }

    public static Sprite GetFood(ESentiment sentiment) => _curConfigData.Foods.GetSprite(sentiment);

    public static Sprite GetTrap(ESentiment sentiment) => _curConfigData.Traps.GetSprite(sentiment);

    public static Sprite GetAvatarByIndex(int index, ESentiment sentiment)
    {
        return _curConfigData.AvatarSO.Avatars[index].GetSprite(sentiment);
    }

    public static Sprite GetRandomAvatar(out int index, ESentiment sentiment)
    {
        index = Random.Range(0, _curConfigData.AvatarSO.Avatars.Length);
        return GetAvatarByIndex(index, sentiment);
    }

    public static Sprite GetRandomSmileSprite(ESentiment sentiment)
    {
        return _curConfigData.Smiles.GetSprite(sentiment);
    }

    public static Sprite GetRandomFoodSprite(ESentiment sentiment)
    {
        return _curConfigData.Foods.GetSprite(sentiment);
    }

    public static Sprite GetRandomTrapSprite(ESentiment sentiment)
    {
        return _curConfigData.Traps.GetSprite(sentiment);
    }
}
