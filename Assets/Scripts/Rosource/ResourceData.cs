﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new Resource Data", menuName = "Configs/ResourceData")]
public class ResourceData : ScriptableObject
{
    [Header("Emoji Sprites")]
    public SOSentiment Smiles;
    public SOSentiment Foods;
    public SOSentiment Traps;

    [Header("Avatar")]
    public SOAvatar AvatarSO;
}
