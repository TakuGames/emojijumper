﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class Health : MonoBehaviour
{
    [SerializeField] private SOFood _foodConfig;
    private Rigidbody2D _rigidbody2D;

    private void Start()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
    }

    public void AddHealth()
    {
        transform.localScale += Vector3.one * _foodConfig.AddedSize;
        _rigidbody2D.mass += _foodConfig.AddedMass;
    }
    
    public void RemoveHealth()
    {
        transform.localScale -= Vector3.one * _foodConfig.AddedSize;
        if (transform.localScale.x < _foodConfig.MinSize)
        {
            gameObject.SetActive(false);
            GameController.GameOverCheck();
            return;
        }

        _rigidbody2D.mass -= _foodConfig.AddedMass;
    }
}
