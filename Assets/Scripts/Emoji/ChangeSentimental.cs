﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeSentimental : MonoBehaviour
{

    private SentimentController _sentimentController;

    void Start()
    {
        _sentimentController = GetComponent<SentimentController>();
    }

    //private void OnTriggerEnter2D(Collider2D collider)
    //{
    //    if (collider.tag != "Player") return;

    //    SentimentController sentimentPlayerController = collider.gameObject.GetComponent<SentimentController>();

    //    if (sentimentPlayerController.Sentiment != _sentimentController.Sentiment)
    //    {
    //        _sentimentController.Sentiment = sentimentPlayerController.Sentiment;

    //    }
        
    //}

    public void TriggerEnter(Collider2D collider)
    {
        //OnTriggerEnter2D(collider);
        if (collider.tag != "Player") return;

        SentimentController sentimentPlayerController = collider.gameObject.GetComponent<SentimentController>();

        if (sentimentPlayerController.Sentiment != _sentimentController.Sentiment)
        {
            _sentimentController.Sentiment = sentimentPlayerController.Sentiment;

        }
    }
}
