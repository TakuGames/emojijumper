﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SentimentController : MonoBehaviour
{
    [SerializeField] private SpriteRenderer _art;
    [SerializeField] private SOSentiment _soSentiment;
    [SerializeField] private ESentiment _sentiment;

    private void Awake()
    {
        Sentiment = _sentiment;
    }

    public ESentiment Sentiment
    {
        get => _sentiment;
        set
        {
            _sentiment = value;
            _art.sprite = _soSentiment.GetSprite(value);
        }
    }

   public Sprite Sprite
    {
        set
        {
            _art.sprite = value;
        }
    }
}
