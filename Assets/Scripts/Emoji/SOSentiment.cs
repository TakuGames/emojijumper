﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "new Sentiment Config", menuName = "Configs/SentimentConfig")]
public class SOSentiment : ScriptableObject
{
    public Sprite[] SentimentsGood;
    public Sprite[] SentimentsBad;


    public Sprite GetSprite(ESentiment sentiment)
    {
        if (sentiment == ESentiment.Good)
            return SentimentsGood[Random.Range(0, SentimentsGood.Length)];
        else
            return SentimentsBad[Random.Range(0, SentimentsGood.Length)];
    }
}


