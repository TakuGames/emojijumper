﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ACharacterMovement : ICharacterMovement
{

    protected Rigidbody2D _rigidbody;
    protected float _horizontalSpeed;
    protected float _maxVerticalSpeed;

    private Vector3 _fP;

    public ACharacterMovement(Rigidbody2D rigidbody, float horizontalSpeed, float maxVerticalSpeed)
    {
        _rigidbody = rigidbody;
        _horizontalSpeed = horizontalSpeed;
        _maxVerticalSpeed = maxVerticalSpeed;
    }

    public abstract void Move();


    public virtual void SwaipJump(float minSwaipDistance, float forceJump)
    {
        if (Input.GetMouseButtonDown(0))
            _fP = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        if (Input.GetMouseButtonUp(0))
        {
            Vector3 sp = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            if (sp.y - _fP.y > minSwaipDistance)
            {
                _rigidbody.velocity = new Vector2(_rigidbody.velocity.x, 0);
                _rigidbody.AddForce(Vector2.up * forceJump , ForceMode2D.Impulse);
            }

        }
    }
}
