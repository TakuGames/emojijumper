﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlByMouse : ACharacterMovement
{
    private Camera _cam;
    private Transform _transform;


    public ControlByMouse(Rigidbody2D rigidbody,float horizontalSpeed,  float maxVerticalSpeed, Camera cam, Transform transform) : base(rigidbody, horizontalSpeed, maxVerticalSpeed)
    {
        _transform = transform;
        _cam = cam;
    }

    public override void Move()
    {
        if (Input.GetMouseButton(0))
        {
            Vector3 targetPosition = _cam.ScreenToWorldPoint(Input.mousePosition);

            float verticalVelocity;
            Vector3 vector = (targetPosition - _transform.position).normalized;

            if (targetPosition.y < _transform.position.y)
                verticalVelocity = _rigidbody.velocity.y;
            else
                verticalVelocity = Mathf.Clamp(vector.y * _horizontalSpeed, float.MinValue, _maxVerticalSpeed);

  
            _rigidbody.velocity = new Vector2(vector.x * _horizontalSpeed, verticalVelocity);
            Debug.Log(_rigidbody.velocity);

            float rot_z = Mathf.Atan2(vector.y, vector.x) * Mathf.Rad2Deg;
            _transform.rotation = Quaternion.Euler(0f, 0f, rot_z - 90);
        }
    }


    public override void SwaipJump(float minSwaipDistance, float forceJump)
    {
        
    }
}
