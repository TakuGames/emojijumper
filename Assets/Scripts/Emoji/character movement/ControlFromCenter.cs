﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlFromCenter : ACharacterMovement
{
  
    private Camera _cam;
    private float _width;


    public ControlFromCenter(Rigidbody2D rigidbody, float horizontalSpeed, float maxVerticalSpeed, Camera cam) : base(rigidbody, horizontalSpeed, maxVerticalSpeed)
    {   
        _cam = cam;
        float height = 2f * _cam.orthographicSize;
        _width = height * _cam.aspect;
    }


    public ControlFromCenter(Rigidbody2D rigidbody, float horizontalSpeed, float maxVerticalSpeed) :this(rigidbody, horizontalSpeed, maxVerticalSpeed, Camera.main)  {}

    public override void Move()
    {
        if (Input.GetMouseButton(0))
        {
            float inputHorizontal = _cam.ScreenToWorldPoint(Input.mousePosition).x / (_width / 2f);
            _rigidbody.velocity = new Vector2(_horizontalSpeed * inputHorizontal, Mathf.Clamp(_rigidbody.velocity.y, float.MinValue, _maxVerticalSpeed));
            Debug.Log("Move " + _rigidbody.velocity.y);
        }
        else
        {
            _rigidbody.velocity = new Vector2(_rigidbody.velocity.x, Mathf.Clamp(_rigidbody.velocity.y, float.MinValue, _maxVerticalSpeed));
        }
    }
 
}
