﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICharacterMovement 
{
    void Move();
    void SwaipJump(float minSwaipDistance, float forceJump);
}
