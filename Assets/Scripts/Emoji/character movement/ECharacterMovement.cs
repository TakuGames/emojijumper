﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ECharacterMovement
{
    Jostick,
    FromCenter,
    ByMouse,
    HorizontalMouse
}
