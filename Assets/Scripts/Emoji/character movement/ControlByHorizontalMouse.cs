﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlByHorizontalMouse : ACharacterMovement
{
    private Camera _cam;
    private Transform _transform;


    public ControlByHorizontalMouse(Rigidbody2D rigidbody, float horizontalSpeed, float maxVerticalSpeed, Camera cam, Transform transform):base(rigidbody,horizontalSpeed, maxVerticalSpeed)
    {    
        _transform = transform;
        _cam = cam;
    }

    public override void Move()
    {
        if (Input.GetMouseButton(0))
        {
            Vector3 targetPosition = _cam.ScreenToWorldPoint(Input.mousePosition);

            Vector3 vector = (targetPosition - _transform.position);
            vector.x = vector.x > 1 ? 1 : vector.x < -1 ? -1 : 0;

            _rigidbody.velocity = new Vector2(vector.x  * _horizontalSpeed, Mathf.Clamp(_rigidbody.velocity.y, float.MinValue, _maxVerticalSpeed));
            Debug.Log(_rigidbody.velocity);

            //float rot_z = Mathf.Atan2(vector.y, vector.x) * Mathf.Rad2Deg;
            //_transform.rotation = Quaternion.Euler(0f, 0f, rot_z - 90);
        }
        else
        {
            _rigidbody.velocity = new Vector2(_rigidbody.velocity.x, Mathf.Clamp(_rigidbody.velocity.y, float.MinValue, _maxVerticalSpeed));
        }

    }


}
