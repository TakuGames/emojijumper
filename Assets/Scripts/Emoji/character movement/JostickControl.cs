﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JostickControl : ACharacterMovement
{
    public string horizontalAxis = "Horizontal";

    public JostickControl(Rigidbody2D rigidbody, float horizontalSpeed, float maxVerticalSpeed) : base(rigidbody, horizontalSpeed, maxVerticalSpeed) {    }

    public override void Move()
    {
        float inputHorizontal = SimpleInput.GetAxis(horizontalAxis);
        _rigidbody.velocity = new Vector2(_horizontalSpeed * inputHorizontal, Mathf.Clamp(_rigidbody.velocity.y, float.MinValue, _maxVerticalSpeed));
    }

}
