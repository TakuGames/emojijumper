﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public SOPlayerController SOPlayer => _soPlayer;
    public Rigidbody2D Rb => _rigidbody;
    public CircleCollider2D CircleCollider => _collider;

    [SerializeField] private SOPlayerController _soPlayer;
    //[SerializeField] private float _distanceToPlatform;
    [SerializeField] private float _mulSpeedForMouse;

    public string horizontalAxis = "Horizontal";
    private float _inputHorizontal;
    private Rigidbody2D _rigidbody;
    private CircleCollider2D _collider;


    [SerializeField] private float _minSwaipDistance;

    private float _width;
    private Camera _cam;

    private ICharacterMovement _movement;

    private ILaunch _launch;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
        _collider = GetComponent<CircleCollider2D>();
    }

    // Start is called before the first frame update
    void Start()
    {
        _launch = GetComponent<ILaunch>();

        if (tag == "Player")
            PullObjects.AddToPullSmiles(gameObject);

        ChangeMovement(ChangePlayerControl.CharacterMovement);
        ChangePlayerControl.ControllerEvent.AddListener(ChangeMovement);
    }

    // Update is called once per frame
    void Update()
    {
        if (_launch != null)
            _launch.JumpLaunch(this);

        _movement.Move();

        _movement.SwaipJump(_minSwaipDistance, _soPlayer.ForceBigJump);

    }

    private void OnEnable()
    {
        _rigidbody.mass = _soPlayer.StartMass;
        transform.localScale = Vector3.one;
        transform.rotation = Quaternion.Euler(Vector3.zero);
    }


    public void ChangeMovement(ECharacterMovement movement)
    {
        switch (movement)
        {
            case ECharacterMovement.Jostick:
                _movement = new JostickControl(_rigidbody, _soPlayer.HorizontalSpeed, _soPlayer.VerticalSpeed);
                break;
            case ECharacterMovement.FromCenter:
                _movement = new ControlFromCenter(_rigidbody, _soPlayer.HorizontalSpeed, _soPlayer.VerticalSpeed, Camera.main);
                break;
            case ECharacterMovement.ByMouse:
                _movement = new ControlByMouse(_rigidbody, _soPlayer.HorizontalSpeed * _mulSpeedForMouse, _soPlayer.VerticalSpeed, Camera.main, transform);
                break;
            case ECharacterMovement.HorizontalMouse:
                _movement = new ControlByHorizontalMouse(_rigidbody, _soPlayer.HorizontalSpeed * _mulSpeedForMouse, _soPlayer.VerticalSpeed, Camera.main, transform);
                break;
        }
    }

    private void OnDestroy()
    {
        ChangePlayerControl.ControllerEvent.RemoveListener(ChangeMovement);
    }

    //private IEnumerator Jump()
    //{
    //    WaitForEndOfFrame oneFrame = new WaitForEndOfFrame();
    //    while (true)
    //    {
    //        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down, _distanceToPlatform, LayerMask.GetMask("Platforms"));
    //        Debug.DrawLine(transform.position, new Vector2(transform.position.x, transform.position.y + (_distanceToPlatform * Vector2.down.y)), Color.red, 2f);
    //        if (hit.collider != null)
    //        {
    //            if (hit.collider.tag == "Platform")
    //            {
    //                Debug.Log("Jump");
    //                _rigidbody.AddForce(Vector2.up * _soPlayer.ForceJump);
    //            }
    //        }
    //        yield return oneFrame;
    //    }
    //}


    //private void OnCollisionEnter2D(Collision2D collision)
    //{

    //    if (collision.collider.tag == "Platform")
    //        _rigidbody.AddForce(Vector2.up * _soPlayer.ForceJump);
    //    else
    //         if (collision.collider.tag == "Player")
    //    {
    //        Debug.Log("collision withPlayer");
    //        _rigidbody.AddForce(-(collision.transform.position - transform.position).normalized * _soPlayer.PushForceFromPlayer);
    //    }
    //}


}
