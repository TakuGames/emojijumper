﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpBehaviour : MonoBehaviour, ILaunch
{
    [SerializeField] private LayerMask _mask;
    [SerializeField] private ForceMode2D _mode = ForceMode2D.Impulse;
    [SerializeField] private float _distance = 0;

    private Coroutine _cor = null;
    private float _finalDistance = 0;

    public void JumpLaunch(PlayerController controller)
    {
        RaycastHit2D hit = Physics2D.Raycast(controller.transform.position, -Vector2.up, Mathf.Infinity, _mask);

        if (hit.collider)
        {
            _finalDistance = controller.CircleCollider.radius * controller.transform.localScale.y;

            if (controller.transform.position.y - hit.point.y <= _finalDistance + _distance)
            {
                controller.Rb.velocity = Vector2.zero;
                controller.Rb.AddForce(Vector2.up * controller.SOPlayer.ForceJump, _mode);
            }
        }
    }
}
