﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Enemy : MonoBehaviour
{
    private SentimentController _sentimentController;



    void Start()
    {
        _sentimentController = GetComponent<SentimentController>();
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Debug.Log("good");
        //if (collision.collider.tag == "Player")
        //    collision.gameObject.SetActive(false);
        if (collision.collider.tag != "Player") return;

        SentimentController sentimentPlayerController = collision.collider.gameObject.GetComponent<SentimentController>();

        if (sentimentPlayerController.Sentiment != _sentimentController.Sentiment)
        {
            collision.gameObject.SetActive(false);

            GameController.GameOverCheck();
        }
    }
}
