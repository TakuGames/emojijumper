﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "new Health", menuName = "Configs/HealthConfig")]
public class SOFood : ScriptableObject
{
    [SerializeField] public float AddedSize;
    [SerializeField] public float AddedMass;
    [SerializeField] public float MinSize = 0.5f;
}
