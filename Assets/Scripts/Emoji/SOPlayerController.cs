﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new Player Config", menuName = "Configs/PlayerConfig")]
public class SOPlayerController : ScriptableObject
{
    [SerializeField] public float ForceJump;
    [SerializeField] public float ForceBigJump;
    [SerializeField] public float PushForceFromPlayer;
    [SerializeField] public float HorizontalSpeed;
    [SerializeField] public float VerticalSpeed;
    [SerializeField] public float StartMass;
}
