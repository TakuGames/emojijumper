﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitingForFriends : MonoBehaviour
{
    private Rigidbody2D _rigidbody;
    private PlayerController _playerController;
    private Collider2D _collider;
    private ChangeSentimental _playerSentimental;
    private WaitingForFriends _waitingForFriends;
    // Start is called before the first frame update
    void Start()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
        _collider = GetComponent<Collider2D>();
        _playerController = GetComponent<PlayerController>();
        _playerSentimental = GetComponent<ChangeSentimental>();
        _waitingForFriends = this;

        ChangeActive(false);

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("trigger");

        if (collision.tag != "Player") return;

        _playerSentimental.TriggerEnter(collision);
        //_playerController.ChangeMovement(ChangePlayerControl.CharacterMovement);
        ChangeActive(true);
        gameObject.tag = "Player";
        _waitingForFriends.enabled = false;
    }

    private void ChangeActive(bool active)
    {

        

        _collider.isTrigger = !active;
        if (active)
            _rigidbody.bodyType = RigidbodyType2D.Dynamic;
        else
            _rigidbody.bodyType = RigidbodyType2D.Static;
        
        _playerController.enabled = active;

        //_playerSentimental.enabled = active;

        
    }
}
