﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangePlayerSentimental : MonoBehaviour
{


    private SentimentController _sentimentController;



    void Start()
    {
        _sentimentController = GetComponent<SentimentController>();
    }



    private void OnTriggerEnter2D(Collider2D collider)
    {

        if (collider.tag != "Player") return;
        SentimentController sentimentPlayerController = collider.gameObject.GetComponent<SentimentController>();

        if (sentimentPlayerController.Sentiment != _sentimentController.Sentiment)
            sentimentPlayerController.Sentiment = _sentimentController.Sentiment;
        else
            collider.GetComponent<Health>().AddHealth();
        gameObject.SetActive(false);
    }

}
