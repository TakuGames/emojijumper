﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroyer : MonoBehaviour
{
    [SerializeField] private bool _isDestroyer = false;

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            collision.gameObject.SetActive(false);
            GameController.GameOverCheck();
        }

        if (_isDestroyer)
            if (collision.gameObject.tag == "Platform")
                collision.gameObject.SetActive(false);

    }
}
