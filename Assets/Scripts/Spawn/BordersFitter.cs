﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class BordersFitter : MonoBehaviour
{
    [SerializeField] private BoxCollider2D _destroyer = null;
    [SerializeField] private BoxCollider2D _respawner = null;
    [SerializeField] private BoxCollider2D _borderLeft = null;
    [SerializeField] private BoxCollider2D _borderRight = null;

    private SpriteRenderer _spRenderer;

    private void Start()
    {
        _spRenderer = GetComponent<SpriteRenderer>();

        _destroyer.size = new Vector2(_spRenderer.bounds.size.x , _destroyer.size.y);
        _respawner.size = new Vector2(_spRenderer.bounds.size.x, _respawner.size.y);
        _borderLeft.size = new Vector2(_borderLeft.size.x , _spRenderer.bounds.size.y);
        _borderRight.size = new Vector2(_borderRight.size.x, _spRenderer.bounds.size.y);

        _destroyer.transform.position = new Vector2(0, _spRenderer.bounds.max.y + _destroyer.size.y / 2);
        _respawner.transform.position = new Vector2(0, _spRenderer.bounds.min.y - _destroyer.size.y / 2);
    }
}
