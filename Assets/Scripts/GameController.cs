﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public static void GameOverCheck()
    {
        if (PullObjects.GetActiveSmileCount() <= 0)
            SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().name);
    }
}
