﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public struct AvatarStruct
{
    public Sprite BadSprite;
    public Sprite GoodSprite;

    public Sprite GetSprite(ESentiment sentiment) =>  sentiment == ESentiment.Good? GoodSprite : BadSprite;
}

[CreateAssetMenu(fileName = "new Avatars", menuName = "Configs/AvatarsConfig")]
public class SOAvatar : ScriptableObject
{
    [SerializeField] public AvatarStruct[] Avatars;
    
    //public AvatarStruct this[int index]
    //{
    //    get
    //    {
    //        return Avatars[index];
    //    }
    //}
}
