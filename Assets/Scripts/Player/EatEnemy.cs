﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EatEnemy : MonoBehaviour
{
    private Rigidbody2D _rb;
    private SentimentController _sentiment;
    private Health _health;

    private void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _sentiment = GetComponent<SentimentController>();
        _health = GetComponent<Health>();
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (!other.collider.CompareTag("Player"))
            return;

        var sentimentEnemy = other.gameObject.GetComponent<SentimentController>();
        if (!sentimentEnemy)
            return;
        
        if (sentimentEnemy.Sentiment == _sentiment.Sentiment)
            return;
        
        var rbEnemy = other.gameObject.GetComponent<Rigidbody2D>();
        if (!rbEnemy)
            return;

        if (rbEnemy.mass < _rb.mass)
        {
            rbEnemy.gameObject.SetActive(false);
            _health.AddHealth();
        }
    }
}
